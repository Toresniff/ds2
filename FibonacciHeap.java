/**
 * FibonacciHeap
 *
 * An implementation of fibonacci heap over non-negative integers.
 */
	
public class FibonacciHeap{
	
	private static int links = 0;
	private static int cuts = 0;
	private HeapNode min;
	//System.out.println
	public int numTrees, numNodes, numMarked = 0;
	
	public FibonacciHeap(){
		this.min = null;
	}
	
	/**
	 * public boolean empty()
	 *
	 * precondition: none
	 * 
	 * The method returns true if and only if the heap
	 * is empty.
	 *   
	 */

	public boolean empty(){
		if(this.min == null)
			return true;
		return false;
	}
	
	/**
	 * public HeapNode insert(int key)
	 *
	 * Creates a node (of type HeapNode) which contains the given key, and inserts it into the heap. 
	 */
	
	public HeapNode insert(int key){
		HeapNode node = new HeapNode(key);
		if(this.empty()){
			this.min = node;
			this.numNodes++;
			this.numTrees++;
			return node;
		}
		min.getNext().setPrev(node);
		node.setNext(min.getNext());
		node.setPrev(min);
		min.setNext(node);
		if(this.min.getKey() > key){
			this.min = node;
		}
		this.numNodes++;
		this.numTrees++;
		return node;
	}
	
	/**
	 * public void deleteMin()
	 *
	 * Delete the node containing the minimum key.
	 *
	 */
	
	public void deleteMin(){
		if(this.empty())
			return;
		HeapNode child = this.min.getChild();
		HeapNode lastChild = null;
		if(this.min.getPrev() != this.min){
			if(child != null){
				lastChild = child.getPrev();
				this.min.getPrev().setNext(child);
				this.min.getNext().setPrev(child.getPrev());
				child.getPrev().setNext(this.min.getNext());
				child.setPrev(this.min.getPrev());
			}
			else{
				this.min.getPrev().setNext(this.min.getNext());
				this.min.getNext().setPrev(this.min.getPrev());
			}
		}
		else{
			if(child == null){
				this.min = null;
				this.numTrees--;
				this.numNodes--;
				return;
			}
			else{
				lastChild = child.getPrev();
				this.min.setNext(child);
			}
		}
		if(child != null){
			HeapNode atChild = child;
			atChild.setParent(null);
			this.numTrees++;
			if(atChild != lastChild){
				while(atChild != lastChild){
					atChild.setParent(null);
					this.numTrees++;
					atChild = atChild.getNext();
				}
			}
		}
		this.numTrees--;
		this.numNodes--;
		this.fixTree();
		}
	
	/**
	 * fixes the tree by linking trees of the same degree
	 * also finds new minimum
	 */
	private void arrayInsert(HeapNode[] treeList, HeapNode tree, int index){
		if(treeList[index] == null){
			treeList[index] = tree;
		}
		else{
			arrayInsert(treeList, this.link(tree, treeList[index]), index+1);
			treeList[index] = null;
		}
	}
	
	private void fixTree(){
		if(this.numTrees == 1){
			this.min = this.min.getNext();
			return;
		}
		HeapNode atTree = this.min.getNext();
		HeapNode lastToDo = atTree.getPrev();
		HeapNode[] treeList = new HeapNode[(int) Math.ceil(Math.log10(this.numNodes)/Math.log10(2)) + 1];
		HeapNode nextTree = atTree.getNext();
		arrayInsert(treeList, atTree, atTree.getRank());
		do{
			atTree = nextTree;
			nextTree = atTree.getNext();
			arrayInsert(treeList, atTree, atTree.getRank());
		}while(lastToDo != atTree);
		HeapNode minNode = null;
		for(int i = 0; i < treeList.length; i++){
			if(treeList[i] != null){
				if(minNode == null)
					minNode = treeList[i];
				else{
					if(minNode.getKey() > treeList[i].getKey())
						minNode = treeList[i];
				}
			}
		}
		this.min = minNode;
	}
	
	/**
	 * links 2 nodes assuming they are of the same rank
	 */
	
	private HeapNode link(HeapNode a, HeapNode b){
		FibonacciHeap.links++;
		this.numTrees--;
		if(a.getKey() < b.getKey()){
			b.setParent(a);
			b.getNext().setPrev(b.getPrev());
			b.getPrev().setNext(b.getNext());
			if(a.getChild() != null){
				b.setNext(a.getChild());
				b.setPrev(a.getChild().getPrev());
				a.getChild().getPrev().setNext(b);
				a.getChild().setPrev(b);
				
			}
			else{
				b.setNext(null);
				b.setPrev(null);
			}
			a.setChild(b);
			a.fixRank(1);
			return a;
		}
		else{
			a.setParent(b);
			a.getNext().setPrev(a.getPrev());
			a.getPrev().setNext(a.getNext());
			if(b.getChild() != null){
				a.setNext(b.getChild());
				a.setPrev(b.getChild().getPrev());
				b.getChild().getPrev().setNext(a);
				b.getChild().setPrev(a);
				
			}
			else{
				a.setNext(null);
				a.setPrev(null);
			}
			b.setChild(a);
			b.fixRank(1);
			return b;
		}
	}
	
	/**
	 * public HeapNode findMin()
	 *
	 * Return the node of the heap whose key is minimal. 
	 *
	 */
	
	public HeapNode findMin(){
		return this.min;
    } 
	    
	/**
	 * public void meld (FibonacciHeap heap2)
	 *
	 * Meld the heap with heap2
	 * Only connects the lists doesn't link anything
	 */
	

	public void meld(FibonacciHeap heap2){
		this.numMarked += heap2.numMarked;
		this.numNodes += heap2.numNodes;
		this.numTrees += heap2.numTrees;
		HeapNode lastHere = this.min.getPrev();
		HeapNode minThere = heap2.findMin();
		HeapNode lastThere = minThere.getPrev();
		this.min.setPrev(lastThere);
		lastThere.setNext(this.min);
		minThere.setPrev(lastHere);
		lastHere.setNext(minThere);
		if(minThere.getKey() < this.min.getKey())
			this.min = minThere;
	}
	
	/**
	 * public int size()
	 *
	 * Return the number of elements in the heap
	 *   
	 */
	
	public int size(){
		return this.numNodes;
	}
		
	/**
	 * public int[] countersRep()
	 *
	 * Return a counters array, where the value of the i-th entry is the number of trees of order i in the heap. 
	 * 
	 */
	
	public int[] countersRep(){
		int[] rankArr = new int[(int) Math.ceil(Math.log10(this.numNodes)/Math.log10(2)) + 1];
		if(this.empty())
			return rankArr;
		HeapNode at = this.min;
		rankArr[at.getRank()]++;
		at = at.getNext();
		while(at != this.min){
			rankArr[at.getRank()] ++;
			at = at.getNext();
		}
	    return rankArr;
	}
		
	/**
	 * public void delete(HeapNode x)
	 *
	 * Deletes the node x from the heap. 
	 * 
	 * turns it to min and deletes min
	 */
	
	public void delete(HeapNode x){
		int delta = x.getKey() - this.min.getKey() + 1;
		this.decreaseKey(x, delta);
		this.deleteMin();
	}
	
	/**
	 * public void decreaseKey(HeapNode x, int delta)
	 *
	 * The function decreases the key of the node x by delta. The structure of the heap should be updated
	 * to reflect this chage (for example, the cascading cuts procedure should be applied if needed).
	 */

	public void decreaseKey(HeapNode x, int delta){    
		x.decreaseKey(delta);
		if(x.getParent() == null){
			if(x.getKey() < this.min.getKey()){
				this.min = x;
				return;
			}
		}
		else{
			HeapNode cutThis = x;
			HeapNode parent = x.getParent();
			HeapNode grandparent;
			if(parent.getKey() > x.getKey()){
				while(true){
					this.cut(cutThis);
					if(cutThis.getMark()){
						cutThis.setMark(false);
						this.numMarked--;
					}
					if(parent.getParent() == null)
						break;
					else if(!parent.getMark()){
						parent.setMark(true);
						this.numMarked++;
						break;
					}
					else{
						grandparent = parent.getParent();
						cutThis = parent;
						parent = grandparent;
					}
				}
			}
		}
		this.min = x;
		return;
	}
	
	/**
	 * cuts from parent and adds to tree list
	 */
	
	private void cut(HeapNode child){
		FibonacciHeap.cuts++;
		this.numTrees++;
		HeapNode parent = child.getParent();
		if(parent.getChild() == child){
			if(child.getNext() != child){
				parent.setChild(child.getNext());
			}
			else
				parent.setChild(null);
		}
		parent.fixRank(-1);
		child.setParent(null);
		if(child.getNext() != child){
			if(child.getPrev() != child.getNext()){
				child.getNext().setPrev(child.getPrev());
				child.getPrev().setNext(child.getNext());
				child.setPrev(null);
				child.setNext(null);
			}
			child.getNext().setPrev(null);
			child.getNext().setNext(null);
			child.setNext(null);
			child.setPrev(null);
		}
		min.getNext().setPrev(child);
		child.setNext(min.getNext());
		child.setPrev(min);
		min.setNext(child);
	}
	
	/**
	 * public int potential() 
	 *
	 * This function returns the current potential of the heap, which is:
	 * Potential = #trees + 2*#marked
	 * The potential equals to the number of trees in the heap plus twice the number of marked nodes in the heap. 
	 */

	public int potential(){    
		return this.numTrees + 2*this.numMarked;
	}
	
	/**
	 * public static int totalLinks() 
	 *
	 * This static function returns the total number of link operations made during the run-time of the program.
	 * A link operation is the operation which gets as input two trees of the same rank, and generates a tree of 
	 * rank bigger by one, by hanging the tree which has larger value in its root on the tree which has smaller value 
	 * in its root.
	 */
	
	public static int totalLinks(){    
		return links;
	}
	
	/**
	 * public static int totalCuts() 
	 *
	 * This static function returns the total number of cut operations made during the run-time of the program.
	 * A cut operation is the operation which diconnects a subtree from its parent (during decreaseKey/delete methods). 
	 */
	
	public static int totalCuts(){    
		return cuts;
	}

}
