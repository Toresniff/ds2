/**
 * public class HeapNode
 * 
 * If you wish to implement classes other than FibonacciHeap
 * (for example HeapNode), do it in this file, not in 
 * another file 
 *  
 */
    
public class HeapNode{

	public int key;
	private int rank = 0;
	private boolean mark = false;
	private HeapNode child = null;
	private HeapNode next = null;
	private HeapNode prev = null;
	private HeapNode parent = null;
	
  	public HeapNode(int key){
  		this.key = key;
  	}

  	public void fixRank(int a){
  		this.rank += a;
  	}
  	
  	public int getKey(){
  		return this.key;
  	}
	
  	public void decreaseKey(int delta){
  		this.key -= delta;
  	}
  	
	public int getRank(){
		return this.rank;
	}
	
	public void setRank(int n){
		this.rank = n;
	}
	
	public boolean getMark(){
		return this.mark;
	}
	
	public void setMark(boolean r){
		this.mark = r;
	}
	
	public HeapNode getChild(){
		return this.child;
	}
	
	public void setChild(HeapNode c){
		this.child = c;
	}
	
	public HeapNode getNext(){
		if(this.next == null)
			return this;
		return this.next;
	}
	
	public void setNext(HeapNode t){
		this.next = t;
	}

	public HeapNode getPrev(){
		if(this.prev == null)
			return this;
		return this.prev;
	}
	
	public void setPrev(HeapNode t){
		this.prev = t;
	}
	
	public HeapNode getParent(){
		return this.parent;
	}
	
	public void setParent(HeapNode t){
		this.parent = t;
	}
}