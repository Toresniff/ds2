
public class Main{
	public static void main(String[]args){
		FibonacciHeap heap = new FibonacciHeap();
		long before = System.currentTimeMillis();
		HeapNode[] nodeArray = new HeapNode[50000];
		HeapNode x = null;
		for(int i = 10000; i >= 1; i--){
			x = heap.insert(i);
			nodeArray[i-1] = x;
		}
		for(int i = 10000; i >= 5000; i--){
//			heap.delete(nodeArray[i-1]);
			heap.deleteMin();
			System.out.println(FibonacciHeap.totalLinks() + " " + heap.numTrees);
			
		}
		long after = System.currentTimeMillis();
		long time = after - before;
		System.out.println("Run Time: " + time + ", Links: " + FibonacciHeap.totalLinks() + ", Cuts: " + FibonacciHeap.totalCuts() + ", potential: " + heap.potential());
	}
}